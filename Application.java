public class Application
{
public static void main(String[]args)
{
	Student s1 = new Student();
	Student s2 = new Student();
	s1.name="david";
	s2.name="andrew";
	s1.id=2145456;
	s2.id=2236961;
	s1.program="Computer Science";
	s2.program="Arts";
	
	s1.sayName();
	s1.sayProgram();
	s2.sayName();
	s2.sayProgram();
	
	Student[] section3 =new Student[3];
	
	section3[0]=s1;
	System.out.println(section3[0].name);
	section3[1]=s2;
	System.out.println(section3[1].name);
	
	
	section3[2]=new Student();
	section3[2].name = "George";
	System.out.println(section3[2].name);
}
}
