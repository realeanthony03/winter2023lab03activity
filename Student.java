public class Student
{
	public int id;
	public String program;
	public String name;
	
	public void sayName()
	{
		System.out.println("My name is " + name + " and my student id is " + id);
	}
	
	public void sayProgram()
	{
		System.out.println("im enrolled in " + program + " program");
	}
}
